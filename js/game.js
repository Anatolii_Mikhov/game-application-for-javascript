var canvas = document.getElementById('game');
var context = canvas.getContext('2d');
var gameStarted = false;
var friction = 0.8;
var gravity = 0.9;

var keys = [];

var player = {
    x: 5,
    y: canvas.height - 20,
    width: 30,
    height: 30,
    speed: 5,
    velX: 0,
    velY: 0,
    jumping: false,
    jumpStrenght: 7,
    color: "#00ff00",
    draw: function(){
    context.fillStyle = this.color;
    context.fillRect(this.x, this.y, this.width, this.height);
    }
};

document.body.addEventListener('keydown',function (event) {
    if (event.keyCode == 13 &&  !gameStarted) {
        startGame();
    }

    keys[event.keyCode] = true;
});

document.body.addEventListener('keyup',function (event) {
    keys[event.keyCode] = false;
});

intro_screen();

function  intro_screen() {
    // Name of the game
    context.font = '50px Lato';
    context.fillStyle = "#cc2366";
    context.textAlign = "center";
    context.fillText("HTML5 Game", canvas.width / 2, canvas.height / 2);
// Invitation to the game
    context.font = '20px Lato';
    context.fillText("Press Enter To Start", canvas.width / 2, canvas.height / 2 + 50);
    context.font = '12px Lato';
    context.fillStyle = "#cc0e17";
    context.fillText("Разработано: Михов Анатолий Фёдорович", canvas.width / 2, canvas.height - 20);
    context.fillText("Ver 0.0.1", canvas.width / 2, canvas.height - 5);
// Game start code
}

function startGame() {
    gameStarted = true;
    clearCanvas();
    setInterval(function () {
        // console.log('startGame')
        clearCanvas();
        loop();
    }, 1000 / 30);
}

function loop() {
    // console.log('start game');
    player.draw();
    // Jumping up
    if(keys[87] || keys[74]){
        if(!player.jumping){
            player.velY = -player.jumpStrenght * 2;
            player.jumping = true;
        }

    }
    // Move right
    if(keys[68]){
        // console.log('right arrow');
        if (player.velX < player.speed){
            player.velX++;
        }

    }
    // Move left
    if(keys[65]){
        // console.log('left arrow');
        if (player.velX > -player.speed){
            player.velX--;
        }

    }
    player.x += player.velX;
    // console.log(player.x);
    player.velX *= friction;

    player.y += player.velY;
    player.velY += gravity;

    if (player.y >= canvas.height - player.height){
        player.y = canvas.height - player.height;
        player.jumping = false;
    }

}

function clearCanvas() {
    context.clearRect(0,0, canvas.width,canvas.height);
}